allow_scm_jenkinsfile = false


application_environments{
    dev{
        ip_addresses = [ "0.0.0.1", "0.0.0.2" ]
    }
    prod{
        long_name = "Production"
        ip_addresses = [ "0.0.1.1", "0.0.1.2", "0.0.1.3", "0.0.1.4" ]
    }
}


/*
  specify which libraries to load: 
    In the Governance Tier configuration file, 
    these should be configurations common across 
    all apps governed by this config. 
*/
libraries{
  merge = true
  sonarqube
  ansible
}

stages{
    continuous_integration{
        unit_test
        build
        static_code_analysis
    }
}

/*
steps{
    unit_test{
        stage = "Unit Test"
        image = "maven"
        command = "mvn -v"
    }
}
*/